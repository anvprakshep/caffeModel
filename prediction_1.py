'''
Title           :Predictions_1.py
Description     :Makes predictions using caffe_model_1 and generates a csv file.
Author          :Nitin Shukla
Date Created    :20170630
'''

import os
import glob
import sys
import requests
import json
sys.path.append('/home/ubuntu/caffe/python')

import cv2
import caffe
import lmdb
import numpy as np
from caffe.proto import caffe_pb2

from flask import Flask, render_template, request, redirect

app = Flask(__name__)

caffe.set_mode_cpu()

#Spatial dimension
IMG_WIDTH = 227
IMG_HEIGHT = 227

#CAFFE NET DEFINITON
#Read model architecture and trained model's weights
net = caffe.Net('/home/ubuntu/DeepLearning_crop_classification/Crop_Classification/caffe_models/caffe_model_1/deploy_1.prototxt',
                '/home/ubuntu/DeepLearning_crop_classification/Crop_Classification/caffe_models/caffe_model_1_iter_3500.caffemodel',
                caffe.TEST)

'''
Reading mean image, caffe model and its weights
'''
#Read mean image
mean_blob = caffe_pb2.BlobProto()
with open('/home/ubuntu/DeepLearning_crop_classification/input_2/mean.binaryproto') as f:
    mean_blob.ParseFromString(f.read())
mean_array = np.asarray(mean_blob.data, dtype=np.float32).reshape(
    (mean_blob.channels, mean_blob.height, mean_blob.width))
'''
Processing
'''

@app.route("/", methods=['GET'])
def main():
    #Define image transformers
    print "Logged here"
    transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
    transformer.set_mean('data', mean_array)
    transformer.set_transpose('data', (2,0,1))

    '''
    Making predicitions
    '''
    #Reading image paths
    # image_name = request.args.get('image_name')
    # url = requests.args.get('url')
    url = 'http://thorium.prakshep.com/img/prakshep_admin.jpg'
    image_name = 'prakshep_admin.jpg'
    i = 0
    while download_image(url,image_name) == 'false' :
        i = i +1 ;
        if(i == 5) :
            print "Failed after 5 tries"
            data = {"status":"500","message":"Image Download failed"}
            return json.dumps(data)

    test_img_paths = [os.path.join("images/", image_name)]
    print test_img_paths, "print test img paths"
    #Making predictions
    test_ids = []
    preds = []
    for img_path in test_img_paths:
        img = cv2.imread(img_path, cv2.IMREAD_COLOR)
        img = transform_img(img, img_width=IMG_WIDTH, img_height = IMG_HEIGHT)

        net.blobs['data'].data[...] = transformer.preprocess('data', img)
        out = net.forward()
        pred_probas = out['prob']

        test_ids = test_ids + [img_path.split('/')[-1][:-4]]
        preds = preds + [pred_probas.argmax()]

        print img_path
        print pred_probas.argmax()
        print '-------'

    '''
    Making submission file
    '''
    print test_ids[0], str(preds[0])
    # with open("/home/ubuntu/DeepLearning_crop_classification/Crop_Classification/caffe_models/caffe_model_1/caffe_model_1.csv","w") as f:
    #     f.write("id,label\n")
    #     for i in range(len(test_ids)):
            # f.write(str(test_ids[i])+","+str(preds[i])+"\n")

    f.close()
    return str(preds[0])

def transform_img(img, img_width=IMG_WIDTH, img_height=IMG_HEIGHT):
    print "transform_img function called"
    #Histogram Equalization
    img[:, :, 0] = cv2.equalizeHist(img[:, :, 0])
    img[:, :, 1] = cv2.equalizeHist(img[:, :, 1])
    img[:, :, 2] = cv2.equalizeHist(img[:, :, 2])

    #Resizing
    img = cv2.resize(img, (img_width, img_height), interpolation = cv2.INTER_CUBIC)

    return img

def download_image(url,image_name):
    print(url)
    try:
        image_path = os.path.join("images/", image_name)
        r = requests.get(url)
        with open(image_path, "wb") as code:
            code.write(r.content)
    except ValueError :
        print("Invalid URL !")
        return "false"
    except :
        print(sys.exc_info())
        print("Unknown Exception" + str(sys.exc_info()[0]))
        return "false"
    return "true"

if __name__ == "__main__" :
    app.run(host="0.0.0.0",port=5000)
